$(document).ready(function() {
    var textElement = $('.main .text span');

    $("body").vegas({
        slides: [
            { src: "img/1.jpg" },
            { src: "img/2.jpg" }
        ],
        timer: false
    });

    textElement.eq(0).novacancy({
        'reblinkProbability': 0.1,
        'blinkMin': 0.2,
        'blinkMax': 0.6,
        'loopMin': 8,
        'loopMax': 10,
        'color': '#00ff00',
        'glow': ['0 0 80px #ffffff', '0 0 30px #008000', '0 0 6px #00ff00']
    });

    textElement.eq(1).novacancy({
        'reblinkProbability': 0.1,
        'blinkMin': 0.2,
        'blinkMax': 0.6,
        'loopMin': 8,
        'loopMax': 10,
        'color': '#ffff00',
        'glow': ['0 0 80px #ffff00', '0 0 30px #808000', '0 0 6px #ffff00']
    });

    textElement.eq(2).novacancy({
        'blink': 1,
        'off': 1,
        'color': 'White',
        'glow': ['0 0 80px White', '0 0 30px Gray', '0 0 6px Gray']
    });

    textElement.eq(3).novacancy({
        'reblinkProbability': 0.1,
        'blinkMin': 0.2,
        'blinkMax': 0.6,
        'loopMin': 8,
        'loopMax': 10,
        'color': '#ff0000',
        'glow': ['0 0 80px #ffffff', '0 0 30px #800000', '0 0 6px #ff0000']
    });
});